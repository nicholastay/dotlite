# dotlite

Lightweight dotfiles with git bare repo - trimmed down from my main dots so it can be put on servers and/or work computers.

Probably will have some basic CLI setup + a really simple i3 config with i3status.

```
$ git clone --bare https://github.com/nicholastay/dotlite.git ~/.dotlite.git
$ git --git-dir=$HOME/.dotlite.git/ --work-tree=$HOME checkout
$ git --git-dir=$HOME/.dotlite.git/ --work-tree=$HOME config --local status.showUntrackedFiles no

# To add more files (once aliases loaded)
# May need to add to gitignore to not use -f (safety)
$ d a ~/.config/...

# Any other git operations
$ d ...
```

## Package notes

General:

- nvim/emacs
- bash/zsh

For Linux Desktop:

- i3
- dmenu
- urxvt/xterm/alacritty
- xrdb
- xset
- setxkbmap

For macOS Desktop:

- yabai
- skhd
