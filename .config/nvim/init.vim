" Nick's light (n)vimrc
" No plug stuff, just the basics

let mapleader="\\"

set nocompatible
filetype plugin on
syntax on
set encoding=utf-8
set number relativenumber

set nohlsearch
set splitbelow splitright
set scrolloff=6

colorscheme peachpuff

set tabstop=4
set softtabstop=0 noexpandtab
set shiftwidth=4

set mouse=a

" Jump to a placeholder character
map <leader><leader> <Esc>/<++><Enter>"_d4lzzi
nnoremap <leader><leader> <Esc>/<++><Enter>"_d4lzzi
inoremap <leader><leader> <Esc>/<++><Enter>"_d4lzzi

" Splits
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Copy + paste
vnoremap <leader>c "+y
vnoremap <leader>x "*y
map <leader>v "+P

" Paste mode
set pastetoggle=<F12>

" Replace all
nnoremap S :%s//g<Left><Left>

" Nice little helper for saving sudo when forget
cmap w!! w !sudo tee >/dev/null %
