# Nick's light .bash_profile
# Load generic profile + bashrc

[ -f ~/.profile ] && . ~/.profile
[ -f ~/.bashrc ] && . ~/.bashrc
